#!/usr/bin/python3

#    Keitlib -- Access to Keithley 3390 signal generator.
#    Copyright (C) 2022 Florin Boariu and Matthias Roessle.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from keithlib import keithley
from os import environ as env
import pytest

@pytest.fixture
def kdev():
    try:
        return keithley.Keith3390()
    except KeyError:
        print ("For device-live unit testing you need to export the IP "
               "address of the device in the environment variable KEITH3390_ADDRESS.")
        raise


def test_keith3390_prop(kdev):
    '''
    ACHTUNG, this is a live test. It will mess up settings of your device.
    But it's not very likely to actually break it.
    '''

    assert len(kdev.id) > 0

    kdev.burstmode = 3
    assert kdev.burstmode == 3

    kdev.burstmode = False
    assert kdev.burstmode == 0

    kdev.output = 'on'
    assert kdev.output 

    kdev.output = 'off'
    assert not kdev.output

    
def test_keith3390_waveform(kdev):
    
    kdev.waveform = "SIN"
    assert kdev.waveform['type'] == 'SIN'

    assert len(kdev.catalogue) > 0

    for c in kdev.catalogue:
        kdev.waveform = { 'type': 'user', 'user': c }
        assert kdev.waveform['type'].upper() == 'USER'
        assert kdev.waveform['user'].upper() == c
        
    kdev.waveform = { 'type': 'ramp', 'symm': 50 }
    assert kdev.waveform['type'] == 'RAMP'


def test_keith3390_volwave(kdev):
    '''
    Manipulating the volatile waveform data.
    '''
    # load stored waveform from memory
    kdev. waveform = {'type': 'USER', 'user': 'PUND40'}
    assert kdev.waveform['user'] == 'PUND40'
    kdev.waveform = {'type': 'user', 'user': 'volatile'}
    assert kdev.waveform['user'] == 'VOLATILE'
    kdev.waveform = {'type': 'user', 'user': 'volatile', 'data': '0,1,0'}
    assert kdev.waveform['user'] == 'VOLATILE'
    kdev.waveform = keithley.pundAsymWave(0.5, 30, 0.3, 20)
    assert kdev.waveform['user'] == 'VOLATILE'


def test_keith3390_propbranch(kdev):
    '''
    Testing the automated PropertyBranch thingy
    '''

    assert type(kdev.burst.state.get()) == int
    assert len(kdev.func.get()) > 0
    
    kdev.outp.set(True)
    assert kdev.outp.get()
    
