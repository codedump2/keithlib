#!/usr/bin/python3

#    Keitlib -- Access to Keithley 3390 signal generator.
#    Copyright (C) 2022 Florin Boariu and Matthias Roessle.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

class MockPropertyBranch(object):
    '''
    Accepts any property.
    '''
    def __init__(self, *args, **kwargs):
        self._val = 3.14

    def get(self):
        return self._val

    def set(self, val):
        self._val = val

    def enable(self):
        self.set(1.0)

    def disable(self):
        self.set(0.0)

    def __getattr__(self, name):
        if name[0] == "_":
            raise AttributError("Nope.")
        return MockPropertyBranch()


class MockKeith3390(object):
    '''
    Mocking class for Keith3390. Has some defauls for
    the explicit attributes (output, waveform, ...)
    '''

    def __init__(self, *args, **kwargs):
        self.output = True
        self.waveform = { "type": "mock" }
        self.burstmode = 37
        self.catalogue = [ "MOCK" ]
        self.id = "Mock Keith"

    def __getattr__(self, name):
        if name[0] == "_":
            raise AttributError("No No.")

        return MockPropertyBranch()
