#!/usr/bin/python3

#    Keitlib -- Access to Keithley 3390 signal generator.
#    Copyright (C) 2022 Florin Boariu and Matthias Roessle.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
import numpy as np

import pyvisa

from os import environ as env

class PropertyBranch(object):
    '''
    The Keithley 3390 protocol is in many details a hierachical
    one -- commands are being built by extending on a base string
    with sub-branches. For instance:

      - `FUNC <name>` enables a specific function, but
        `FUNC:<name> <param>` sets a specific parameter
         of that function.

      - `BURST:STATE` sets the burst state `BURST:NCYCLES` sets
        the number of burst cycles, etc.

    This object takes any parameter that doesn't begin with an underscore
    and interprets it as a command to send to Keithley on top of the base
    command that it was initiated with. For instance:
    ```
       p = PropertyBranch(kdev, 'burst')
       p.state = "off"
    ```

    This would return the result of the `"BURST:STATE OFF" command.
    Any strings sent are converted to upper case. Any boolean parameters
    (True/False) are converted to "ON" / "OFF".

    In returning values, the class tries first to convert "ON"/"OFF" to
    boolean, than any integer values in might encounter, then floating
    points. If all fail, a regular Python string is returned.

    The specific functions `get()` and `set()` act directly on the root
    command, e.g. this would result in setting the function "FUNC SIN",
    then requesting the function name again (`FUNC?`):
    ```
       p = PropertyBranch(kdev, 'func`)
       p.set("sin")
       p.get() ## --> 'SIN'
    ```

    The functions `enable()` and `disable()` are shortcuts for `set("ON")`
    and `set("OFF")`, respectively.

    '''

    def __init__(self, kdev, cmdroot, nxerror=None):
        '''
        Parameters:
          - `kdev`: The Keithley3380 device to use. We defer all
            communication to that device.
          - `cmdroot`: The base string for the command, without
            the trailing colon (e.g. `BURST` or `FUNC`).
        '''

        # check if the attribute exists, raise an nxerror otherwise
        if nxerror:
            try:
                kdev.query(cmdroot+"?")
            except pyvisa.VisaIOError:
                raise nxerror
        
        self.cmdroot = cmdroot
        self.kdev    = kdev
        self.cmdwhitelist = []


    def query(self, subcmd):
        val = self.kdev.query(self.cmdroot+subcmd)

        if val.upper() == "OFF":
            return False
        if val.upper() == "ON":
            return True

        try:
            return int(val)
        except ValueError:
            try:
                return float(val)
            except ValueError:
                return val

    def get(self):
        return self.query("?")

    def set(self, val):
        if type(val) == bool:
            val = "ON" if val==True else "OFF"
        cmd = self.cmdroot+" %s" % str(val)
        ret = self.kdev.write(cmd)
        if ret != len(cmd)+1:
            raise IOError("Error writing: {cmd} (sent %d bytes, "
                          "should be %d)" % (ret, len(cmd)))
        

    def enable(self):
        return self.set("ON")

    def disable(self):
        return self.set("OFF")

    def __getattr__(self, name):
        if name[0] == "_":
            raise AttributError("{name} doesn't look like a Keithley field")

        # Exclude whitelisted commands from raising initial errors
        if name in self.cmdwhitelist or \
           name in [ i.upper() for i in self.cmdwhitelist ]:
            nxerror=None
        else:
            nxerror=AttributeError("%s:%s is no valid field" %\
                                   (self.cmdroot, name))
            
        return PropertyBranch(self.kdev, "%s:%s" % (self.cmdroot, name), nxerror)


class Keith3390(object):
                     

    def __init__(self, ip=None, setup={}, timeout=3.0):
        '''
        Connects via TCP/IP to the waveform generator behind IP address `IP`.
        Default is `None`, which means the address will be read from the
        `KEITH3390_ADDRESS` environment variable.

        Parameters:
          - `ip`: The IP address to connect to. If none is specified, it is
            taken from the `KEITH3390_ADDRESS` environment variable.

          - `setup`: A dictionary containing additional setup. Keys are the
            parameter name (e.g. 'VOLT:UNIT'), values are the value ('Vpp').

          - `timeout`: Timeout in seconds
        '''

        addr = ip or env['KEITH3390_ADDRESS']

        rm = pyvisa.ResourceManager('@py')
        self.dev = rm.open_resource('TCPIP::' + addr + '::INSTR')
        self.dev.read_termination = '\n'
        self.dev.write_termination = '\n'
        self.dev.timeout = int(timeout*1000)
        
        for k, v in setup.items():
            self.dev.write('%s %r' % (k, v))

        # We have a customized __getattr__ which returns a ProperyBranch
        # for automated fields. It checks for the existence of the top
        # branch by executing a phony "CMD?" query on the top level "CMD".
        # Sometimes, the top level "CMD?" raises an error, but lower-level
        # commands work ("BURST?" vs "BURST:STATE?". We whitelist everything
        # that we know to be a valid property here.
        self.cmd_whitelist = [ 'burst' ]


    def write(self, s):
        '''
        Wrapper for the `write()` command that does some
        error hanlding.
        '''
        ret = self.dev.write(s)
        if ret != len(s)+1:
            raise IOError("Wrote %d bytes, whould have been %d (%s)" %\
                          (ret, len(s), str(s)))

    
    def query(self, q):
        '''
        Wrapper for the `query()` command that does some
        error hanlding.
        '''
        return self.dev.query(q)


    @property
    def id(self):
        '''
        Return stuff.
        '''
        return self.query('*IDN?')

    
    def __getattr__(self, name):

        # Eveything tha looks remotely like a private attribute
        # is none of Keithley's.
        if name[0] == "_":
            raise AttributError("{name} doesn't look like a Keithley field")

        # Exclude whitelisted commands from raising initial errors
        if name in self.cmd_whitelist or \
           name in [ i.upper() for i in self.cmd_whitelist ]:
            nxerror=None
        else:
            nxerror=AttributeError("%s no valid field" % name)
            
        return PropertyBranch(self.dev, "%s" % name, nxerror)
    

    #
    # Easy access to a number of interesting functions
    # (burst mode, waveform configuration, output on/off).
    # These all override PropertyBranch automatism only for
    # that specific property names.
    #
    @property
    def burstmode(self):
        '''
        Returns the number of cycles if burst-mode is on,
        or 0 otherwise.
        '''
        burst_on = self.query('BURST:STATE?') == '1'
        if burst_on:
            return int(float(self.query('BURST:NCYCLES?')))
        return 0
    
    @burstmode.setter
    def burstmode(self, numberOfCycles):
        '''
        Configures Keithley 3390 AFG for a very specific type of burst mode
        (externally triggered burst mode with N cycle waveforms.)
        
        Arguments:
          - `numberOfCycles`: how many waveforms are sent after trigger signal is
            received. If set to 0, burstMode is disabled.
        '''
        if numberOfCycles in [0, None, False]:
            self.write('BURS:STAT OFF') # disable burst mode
            return
        
        self.write('BURST:MODE TRIG') # burst mode with triggering
        self.write('BURST:NCYC %d' % (numberOfCycles,)) # send N waveforms
        self.write('TRIG:SOUR EXT') # ensure that triggering is done on external trigger input
        self.write('BURST:STAT ON') # enable burst mode


    @property
    def output(self):
        return self.query('OUTP?') == '1'

    @output.setter
    def output(self, enable):
        '''
        Switch Keithley 3390 AFG output on or off.
        Accepted input is 0, 1, 'on'/'off' and 'ON'/'OFF'
        '''
        self.write('OUTP %s' % ('ON' if enable in [1, True, 'on', 'ON' ] else 'OFF', ))


    @property
    def catalogue(self):
        l = self.query('DATA:CAT?').split(',')
        return [s.strip('"') for s in l]


    @property
    def waveform(self):
        '''
        Returns the current waveform and the main parameters (frequency, amplitude, offset).
        Note that specific parameters (VOLATILE waveform parameters, type-specific parameters
        etc) are not returned.
        '''
        wform = {
            'type':      self.query('FUNC?'),
            'amplitude': float(self.query('VOLT?')),
            'frequency': float(self.query('FREQ?')),
            'offset':    float(self.query('VOLT:OFFS?'))
        }
        
        if wform['type'].upper() == 'USER':
            wform['user'] = self.dev.query('FUNC:USER?')
            
        return wform

    @waveform.setter
    def waveform(self, form):
        '''
        Sets the waveform. This is serious business; the Keithley has several types
        and scenarii of how to load/enable waveforms:
        
          1. Built-in waveforms (SIN, SQUARE, NOISE, DC, RAMP, PULSE)
        
          2. "User-defined" waveforms, of which there are 3 types: pre-defined by
             the manufacturer, available from "catalogue"; defined by the user,
             loadable into catalogue; and defined by the user, loadable into "volatile".

        Available types for "user-defined" are shown by the `catalogue` property
        of this object. As of this writing, these seem to be 'EXP_RISE', 'EXP_FALL',
        'NEG_RAMP', 'SINC', 'CARDIAC', 'ASYMPULS1075', 'PUND40', 'AYMPULSE080', 'AYMPULSE105'.
        
        To each waveform there are three parameters that are always accessible
        and configurable: 'frequency', 'amplitude' and 'offset'.

        Depending on the exact type of the waveform (in particular for the builtin ones),
        there may or may not be extra parameters available. For the 'USER VOLATILE'
        waveform, a specific string has to be loaded which actually describes the waveform.

        This setter takes either a single string specifyin the waveform (i.e.
        one of the builtin types, one of the user-catalogue waveforms, or the
        "USER VOLATILE" waveform) without any parametes, in which case the waveform
        is used with last time's settings; or a dictionary with keywords specifying
        the waveform and parameters. Accepted dictionary keys are as follows (case-sensitive,
        unless stated otherwise):

          - `"type"`: This is a string specifying either one of the built-in waveforms
            (e.g. from above), or `"USER"` if the waveform is one of the user waveforms.

          - `"user"`: If `"type" == "USER"`, then this field specifies the name of the
            user-defined form (i.e. one of the catalog names as presented in `self.catalog`)
            or can be `"VOLATILE"` to specify that the waveform from volatile memory
            can be used.

          - `"frequency"`, `"offset"`, `"amplitude"`: see above, common parameters for all
            wave forms. These are considered for all types of waveforms

          - `"data"`: For `"type" == "USER"` and `"user" == "VOLATILE"`, an extra optional
            field called `"data"` is loaded as the current `USER VOLATILE` waveform
            specification data (consult Keithley documentation for the exact format of the
            string; see also `pundAsymWave()` for a convenient way to define such
            a string.)

          - `"..."`: Any extra key is interpreted as a `FUNC:<type>:<param>` value,
            where `<type>` is the built-in type string, and `<param>` is extracted from
            `...`. These keys are all converted to upper-case before being sent to
            the Keithley device.

        '''

        if isinstance(form, str):
            if form.startswith('user'):
                form = { 'type': 'USER', 'user': form[5:] }
            else:
                form = { 'type': form }
                
    
        if form.get('amplitude'):
            self.dev.write('VOLT %f' + form.get('amplitude'))

        if form.get('frequency'):
            self.dev.write('FREQ %f' + form.get('frequency'))

        if form.get('offset'):
            self.write('VOLT:OFFSET %f' + form.get('offset'))

        wave_params = {k.upper():v for k,v in form.items() if k.lower() not in \
                       ['amplitude', 'frequency', 'type', 'offset', 'user', 'data']}

        wave_type = form.get('type').upper()

        self.write("FUNC %s" % wave_type)
        if wave_type == "USER":
            self.write("FUNC:USER %s" % form['user'])
                              
        for k in wave_params:
            self.write('FUNC:%s:%s ' % (wave_type, k)  +  str(wave_params[k]))

        if wave_type == 'USER' and form['user'].upper() == 'VOLATILE':
            self.write('FUNC USER')
            
            if form.get('data'):
                self.write('DATA VOLATILE,'+ form.get('data'))



def sawToothWave(amplitude, frequency, symmetry=50):
    '''
    Returns a sawtooth wave dictionary for a Keithley 3390 AFG.
    Accepting the following kw arguments
      - `amplitude`: Voltage (as currently configured)
      - `frequency`: Frequency in Hz
      - `symmetry`: Symmetry of the tooth in percent
    '''

    return   { 'type': 'RAMP',
               'SYMM': symmetry,
               'amplitude': amplitude,
               'frequency': frequency }


def triangleWave(amplitude, frequency):
    return sawToothWave(amplitude=amplitue, frequency=frequency, symmetry=50)


def sineWave(amplitude, frequency):
    return { 'type': 'SIN',
             'frequency': frequency,
             'amplitude': amplitude }


def pundAsymWave(A1, d1, A2, d2):
    """
    Generates an asymmetric positive-up negative-down (PUND) sequence in the kHz range
    that can be uploaded to the Keithley 3390 arbitrary function generator.
    
    Parameters:
      - `A1`: relative amplitude of pulse 1 (-1..1)
      - `d1`: duration of pulse 1 in us
      - `A1`: relative amplitude of pulse2  (-1..1)
      - `d1`: duration of pulse 2 in us
    
    Total duration must be < 1000 arbitrary time units.

    Returns a parameter dictionary that can be fed to `Keith3390.waveform`,
    """
    
    # check input
    if A1 < -1 or A1 > 1:
        raise Exception('A1 should not exceed 1. The value of A1 was: {}'.format(A1))
    if A2 < -1 or A2 > 1:
        raise Exception('A1 should not exceed 1. The value of A2 was: {}'.format(A2))
    if 2 * d1 + 2 * d2 > 1000:
        raise Exception('Total duration of the PUND pulses is too long: {} instead of 1000'.format(2 * d1 + 2 * d2))
    
    # generate "time" vector
    signal = np.zeros(1000)
    
    # calculate time where no pulse > 0 is applied
    offset = int((1000 - (2 * d1 + 2 * d2)) / 5)
     
    # set amplitudes for the PUND pulses
    signal[offset:offset+d1] = A1
    signal[2*offset+d1:2*offset+d1+d2] = -A2
    signal[3*offset+d1+d2:3*offset+2*d2+d1] = A2
    signal[4*offset+2*d2+d1:4*offset+2*d2+2*d1] = -A1
 
    # convert to string
    signal_string = ','.join(str(x) for x in signal)

    return {
        'type': 'USER',
        'user': 'VOLATILE',
        'data': signal_string
    }
