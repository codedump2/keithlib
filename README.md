Keithlib
========

Keithlib is -- the name gives it away -- a library for accessing
Keithley signal generators. It was written and tested using the
[Keithley 3390](https://www.tek.com/en/products/arbitrary-waveform-generators/model-3390),
but should be very easily extensible.

Obtaining and installing Keithlib
---------------------------------

Keithlib is currently not in PyPI; as long as that's the
case, the best and only option is to download it directly
from the author's Gitlab site:

```
$ git clone git@gitlab.com:codedump2/keithlib
$ export PYTHONPATH=$PYTHONPATH:$PWD/keithlib/src
$ export KEITH3390_ADDRESS=10.0.0.18
```

The `PYTHONPATH` line will ensure that Python is able to
find the library. You can move Keitlib to a location of
your choosing (e.g. `/opt`, or `~/.local/share/` and
set the paths accordingly).

The only configuration option you have is the IP address
of the Keithley device, which should be set via the environment
variable `KEITH3390_ADDRESS`. Here, it is assumed to be
`10.0.0.18`, but more than likely this will be different
for your setup.

Quick'n Dirty Operation Instructions
------------------------------------

Once Keithlib is installed and the proper variables are all
set up, you're ready to go. This is an example invoking the
Python shell, but you could use it just as well using
IPython, Jupyter Notebook, or a custom script.

Here we're specifying the IP address directly; this is another
way to set up connection:

```
$ python3
Python 3. [...]
>>> from keithlib import keithley
>>> dev = keithley.Keith3390("10.0.0.18")
>>> dev
<keithlib.keithley.Keith3390 object at 0x7fad6fd04520>
```

This means that a successful Keithley object was created.
Most likely you're ready to go (errors would've been reported).


Get acquainted and ask for first connections:
```
>>> dev.id
'Keithley Instruments Inc.,3390,1421555,1.06-0B2-04-07-04'
>>> dev.catalogue
['VOLATILE', 'EXP_RISE', 'EXP_FALL', 'NEG_RAMP', 'SINC', 'CARDIAC', ...]
>>> dev.output
>>> True	
```

Enable / disable output, modify burst mode etc
```
>>> dev.output = False
>>> dev.burstmode = 10
>>> dev.waveform = { "type": "SIN", "frequency": 100, "voltage": 2 }
```

Specific Properties of Keith3390
--------------------------------

A small number of properties for very basic use have been implemented
in `Keith3390`. These are:

  - `id`: Returns the self-reported ID string of the device
  
  - `output`: Essentially turns the output on/off, but does some
    more convenient type conversion (i.e. can be used with `True`,
	`1` or `"ON"`, and returns a boolean value when read)
	
  - `burstmode`: When querried, returns the current number of burst
    cycles if burst mode is on. Otherwise it returns 0. When set,
	it accepts an integer and sets the number of burst cycles to that
	integer, enables the burst mode, *and* sets it to enable on
	external trigger.
	
  - `waveform`: Accepts a dictionary as a waveform configurator.
    The code documentation is fairly comprehensive on how the
	dictionary is supposed to be built. When queried, it returns
	a simplified dicionary (not all parameters are accessible
	to queries)

Automated Access to Properties
------------------------------

Beyond the explicitly defined properties, `Keith3390` has a
catch-all feature that interprets any property accessed as
a keyword in the device's query language. For instance,
of we access `dev.OUTP`, the `OUTP` property is querried;
for `dev.FUNC`, `FUNC` it is.

This also works recursively, so `dev.FUNC.USER` would actually
return the currently active user-supplied custom waveform:

```
>>> dev.FUNC.USER.get()
'VOLATILE'
>>> dev.FUNC.set("USER")
>>> dev.FUNC.USER.set(dev.catalogue[0])
>>> dev.FUNC.USER.get()
'EXP_RISE'
```

Accessing the `OUTP` functions of the device, for instance,
works like this (shown using the explicit property `output`,
and the automated `OUTP` for comparison):

```
>>> dev.OUTP.STATE.get()
0
>>> dev.output
False
>>> dev.OUTP.STATE.set(1)
>>> dev.OUTP.STATE.get()
1
>>> dev.output
True
```

Note that the objects returned by the automated properties
are not the values themselves; they are `keithley.PropertyBranch`
objects, and they offer these functions to manipulate values:
  
  - `set(val)`: set the current value of the blank property,
    e.g. `"PROP val"`.
  
  - `get()`: return the current value of the blank property; tries
    to convert to `bool` if the value is a string that says `"ON"`
	or `"OFF"`, or to a number (`int` or `float`) if possible; if
	not, the string is returned.
  
  - `enable()`/`disable()`: shorthand for `set("ON")` / `set("OFF")`
    (note that not all properties might support this, Keithlib
	has no way of knowing which do or don't).
  
  - `__getattr__()`: this is overloaded to return a sub-`PropertyBranch`
    by the same name; e.g. calling `USER` on the `PropertyBranch`
	`dev.FUNC` will return `dev.FUNC.USER`, i.e. the one accessible
	by the `"FUNC:USER"` set of commands.
	
This allows a wide range of properties to be accessed ad-hoc,
but obviously won't offer a very in-depth error analysis and
checking. Use with caution.

Bugs
----

Of course this software is BugFree(tm). Right? ;-)

If you think you found a bug (which you haven't, because it's
bug free, but probably you are confused and *think* you found one,
so that's ok :-p), feel free to contact the author
(florin.hzb(at)selfmx.net)) or drop an issue at the Gitlab page.
